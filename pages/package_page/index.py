import customtkinter
import json
from widgets import ScrollableLabelButtonFrame
from shared import Configuration

class PackagePage(customtkinter.CTkFrame):
    def __init__(self, master, labelButton = None, updateButton = None, **kwargs):
        super().__init__(master, **kwargs)
        self.grid_columnconfigure(0, weight = 1)

        self.packages_page_frame = customtkinter.CTkScrollableFrame(self,corner_radius=1, fg_color="transparent",width=410, height=410)
        self.packages_page_frame.grid(row=0,column=0)

        self.labelButton = labelButton
        self.updateButton = updateButton
        self.radiobutton_variable = customtkinter.StringVar()

        self.scrollable_label_button_frame = ScrollableLabelButtonFrame(master=self.packages_page_frame, width=400, height = 390, command=self.labelButton, corner_radius=0)
        self.scrollable_label_button_frame.grid(row=0, column=1, padx=10, pady=5, sticky="nsew")

        package_json_path = "../packages/packages.json"

        with open(package_json_path) as file:
            file_content = file.read()

        self.packages = json.loads(file_content)

        for _, (key, value) in enumerate(self.packages.items()):
            self.scrollable_label_button_frame.add_item(f"{key}",state=value["state"], image=customtkinter.CTkImage(Configuration.package_image))

        self.update_button = customtkinter.CTkButton(self, text = "update packages", font=('Arial', 15, 'bold'), command=self.updateButton)
        self.update_button.grid(row=1, column=0,sticky="nsew")

    def change_button(self, item):
        self.scrollable_label_button_frame.change_button(item)
        self.packages[item]["state"] = not self.packages[item]["state"]

    def get_packages(self):
        return self.packages

