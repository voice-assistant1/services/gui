import customtkinter
from shared import Configuration

class HomePage(customtkinter.CTkFrame):
    def __init__(self, master, command = None, **kwargs):
        super().__init__(master, **kwargs)
        self.grid_columnconfigure(0, weight = 1)

        self.command = command
        self.radiobutton_variable = customtkinter.StringVar()

        self.image_label = customtkinter.CTkFrame(self,corner_radius=1, fg_color="transparent",width=410, height=410)
        self.image_label.grid(row=0,column=0,sticky="nsew")

        self.home_page_image = customtkinter.CTkImage(Configuration.home_page_image, size=(380, 215))

        self.home_image = customtkinter.CTkLabel(self.image_label, text = "", image=self.home_page_image)
        self.home_image.grid(row=0, column=0)