import os
from PIL import Image

class Configuration():
    image_path = os.path.join(os.path.dirname(os.path.realpath(__file__ + "\\..\\..\\")), "content")

    icon_path = image_path + "\liz_logo.ico"
    logo_image = Image.open(os.path.join(image_path, "liz_logo.png"))
    large_test_image = Image.open(os.path.join(image_path, "large_test_image.png"))
    image_icon_image = Image.open(os.path.join(image_path, "image_icon_light.png"))
    home_image = Image.open(os.path.join(image_path, "home_light.png"))
    chat_image = Image.open(os.path.join(image_path, "chat_light.png"))
    package_image = Image.open(os.path.join(image_path, "package.png"))
    home_page_image = Image.open(os.path.join(image_path, "home_page.png"))