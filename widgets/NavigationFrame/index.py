import customtkinter

class NavigationFrame(customtkinter.CTkFrame):
    def __init__(self, master, **kwargs):
        super().__init__(master, **kwargs)
        self.grid_columnconfigure(0, weight = 1)

        # self.radiobutton_variable = customtkinter.StringVar()
        self.button_list = []

        self.logo = None
        self.test_button = customtkinter.CTkLabel(self, text="Hello world!", compound="left", padx=5, anchor="w")
        self.test_button.grid(row = 1, column=0, pady=(0, 10), sticky="w")

    
    def add_logo(self, name, image):
        self.logo = customtkinter.CTkLabel(self, text=f'  {name}', image=image,
                                                             compound="left", font=customtkinter.CTkFont(size=30, weight="bold"))
        self.logo.grid(row=0, column=0, padx=20, pady=20)

    def add_label(self, name, image, command):
        button = customtkinter.CTkButton(self, corner_radius=0, height=40, border_spacing=10, text=name,
                                                   fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
                                                   image=image, anchor="w", command=command)
        
        button.grid(row = len(self.button_list) + 1,column = 0, sticky = "ew")

        self.button_list.append(button)

        return button