import customtkinter

class ScrollableLabelButtonFrame(customtkinter.CTkScrollableFrame):
    def __init__(self, master, command=None, **kwargs):
        super().__init__(master, **kwargs)
        self.grid_columnconfigure(0, weight=1)

        self.command = command
        self.radiobutton_variable = customtkinter.StringVar()
        self.label_list = []
        self.button_list = []

    def add_item(self, item, state,  image=None):
        label = customtkinter.CTkLabel(self, text=item, font=("Arial", 18),  image=image, compound="left", padx=5, anchor="w")

        if state == True:
            button = customtkinter.CTkButton(self, text="Included", fg_color = "#398552", hover_color = "#29613b", font=('Arial', 15, 'bold'), width=100, height=24)
        else:
            button = customtkinter.CTkButton(self, text = "Excluded", fg_color = "#ba5454", hover_color="#8a3d3d", font=('Arial', 15, 'bold'), width=100, height=24)
            
        if self.command is not None:
            button.configure(command=lambda: self.command(item))
        label.grid(row=len(self.label_list), column=0, pady=(0, 10), sticky="w")
        button.grid(row=len(self.button_list), column=1, pady=(0, 10), padx=25)
        self.label_list.append(label)
        self.button_list.append(button)

    def remove_item(self, item):
        for label, button in zip(self.label_list, self.button_list):
            if item == label.cget("text"):
                label.destroy()
                button.destroy()
                self.label_list.remove(label)
                self.button_list.remove(button)
                return
            
    def change_button(self, item):
        for label, button in zip(self.label_list, self.button_list):
            if item == label.cget("text"):
                if button.cget("text") == "Included":
                    button.configure(text = "Excluded", fg_color = "#ba5454", hover_color="#8a3d3d", font=('Arial', 15, 'bold'), width=100, height=24)
                    return
                button.configure(text = "Included", fg_color = "#398552", hover_color = "#29613b", font=('Arial', 15, 'bold'), width=100, height=24)
                return
            