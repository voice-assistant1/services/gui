import json
import os

'''
{
    "package_name" : {
        "keywords" : [],
        "state"  : bool,
        "name" : fileName,
        * "logo" : *.png 
    }
}
'''

class PackageLoader():
    def __init__(self) -> None:
        self.package_path = "../packages"

    def generate_json(self):
        packages = {}
        for _, name in enumerate(os.scandir(self.package_path)):
            if name.is_dir():
                with open(self.package_path + f"/{name.name}/keywords.json") as file:
                    file_content = file.read()
                
                file_content = json.loads(file_content)

                tempPackageJson = {
                    **file_content,
                    "state" : True,
                    "name" : name.name
                }

            packages["package_{0}".format(name.name)] = tempPackageJson

        json_object = json.dumps(packages, sort_keys=True, indent=4)

        with open(self.package_path + "/packages.json", "w") as outfile:
            outfile.write(json_object)
