import customtkinter
import json
import os

from widgets import NavigationFrame
from pages import HomePage, PackagePage
from shared import Configuration
from processes import PackageLoader

customtkinter.set_default_color_theme("green")  # Themes: blue (default), dark-blue, green
customtkinter.set_appearance_mode("dark")

class App(customtkinter.CTk):
    def __init__(self):
        super().__init__()

        package_json_path = os.path.exists("../packages/packages.json")
        
        if not package_json_path:
            print("Generating packages...")
            PackageLoader().generate_json()
            while not package_json_path:
                package_json_path = os.path.exists("../packages/packages.json")
                
        self.title("Elizabeth")
        self.geometry("700x450")
        self.resizable(width=False, height=False)

        # set grid layout 1x2
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=1)

        # set logo for application
        self.iconbitmap(Configuration.icon_path)

        self.logo_image = customtkinter.CTkImage(Configuration.logo_image , size=(75, 75))
        self.large_test_image = customtkinter.CTkImage(Configuration.large_test_image, size=(500, 150))
        self.image_icon_image = customtkinter.CTkImage(Configuration.image_icon_image, size=(20, 20))
        self.home_image = customtkinter.CTkImage(Configuration.home_image, size=(20, 20))
        self.chat_image = customtkinter.CTkImage(Configuration.chat_image, size=(20, 20))
        self.package_image = customtkinter.CTkImage(Configuration.package_image, size=(20, 20))


        # create navigation frame
        self.navigation_frame = customtkinter.CTkFrame(self, corner_radius=0)
        self.navigation_frame.grid(row=0, column=0, sticky="nsew")
        self.navigation_frame.grid_rowconfigure(4, weight=1)

        self.navigation_frame_labels = NavigationFrame(master = self.navigation_frame, width = 250, height = 450)
        self.navigation_frame_labels.grid(row=0, column=0, sticky="nsew")

        self.navigation_frame_labels.add_logo(name="Elizabeth", image=self.logo_image)

        self.home_button = self.navigation_frame_labels.add_label(name="Home", image=self.home_image, command=self.home_button_event)
        self.package_button = self.navigation_frame_labels.add_label(name="Packages", image=self.package_image, command=self.package_button_event)
        self.options_button = self.navigation_frame_labels.add_label(name="Options", image=self.chat_image, command=self.option_button_event)

        # Home frame
        self.home_frame = customtkinter.CTkFrame(self, corner_radius=1, fg_color="transparent")

        self.smth = HomePage(master=self.home_frame, width = 455, height = 420)
        self.smth.grid(row=1, column=2, padx=10, pady=5, sticky="nsew")

        # Packages frame
        self.package_frame = customtkinter.CTkFrame(self, corner_radius=1, fg_color="transparent")

        self.package_page = PackagePage(master=self.package_frame, labelButton=self.label_button_frame_event, updateButton=self.update_button_frame_event, width = 425, height = 470)
        self.package_page.grid(row=1, column=2, padx=10, pady=5, sticky="nsew")

        # Options frame
        self.third_frame = customtkinter.CTkFrame(self, corner_radius=0, fg_color="transparent")

        # select default frame
        self.select_frame_by_name("home")

    def label_button_frame_event(self, item):
        self.package_page.change_button(item)

    def update_button_frame_event(self):
        json_object = json.dumps(self.package_page.get_packages(), sort_keys=True, indent=4)
        with open("../packages/packages.json", "w") as outfile:
            outfile.write(json_object)

    def select_frame_by_name(self, name):
        # set button color for selected button
        self.home_button.configure(fg_color=("gray75", "gray25") if name == "home" else "transparent")
        self.package_button.configure(fg_color=("gray75", "gray25") if name == "Packages" else "transparent")
        self.options_button.configure(fg_color=("gray75", "gray25") if name == "Options" else "transparent")

        # show selected frame
        if name == "home":
            self.home_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.home_frame.grid_forget()
        if name == "Packages":
            self.package_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.package_frame.grid_forget()
        if name == "Options":
            self.third_frame.grid(row=0, column=1, sticky="nsew")
        else:
            self.third_frame.grid_forget()

    def home_button_event(self):
        self.select_frame_by_name("home")

    def package_button_event(self):
        self.select_frame_by_name("Packages")

    def option_button_event(self):
        self.select_frame_by_name("Options")

if __name__ == "__main__":
    app = App()
    app.mainloop()

